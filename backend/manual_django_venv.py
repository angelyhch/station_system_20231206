import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'application.settings')
django.setup()


from pathlib import Path
from station_system.views import station, torque, guowangbuliang, jiajutiaozhengtaizhang
from station_system.utils.excelTodb import ExcelToDb

def import_excel_data(path, model):
    # 设置读取的excel文件路径
    cur_path = Path(__file__).resolve().parent
    #station
    excel_path = Path.joinpath(cur_path,path)
    abs_path = str(excel_path.absolute())

    e2d= ExcelToDb()
    df = e2d.read_excel(abs_path)
    print(df)
    e2d.excel_to_db(df, model)

station_path = 'station_system/temp/station_c62x_20230613.xlsx'
station_model = station.Station

torque_path = 'station_system/temp/torque.xlsx'
torque_model = torque.Torque

guowangbuliang_path = 'station_system/temp/guowangbuliang.xlsx'
guowangbuliang_model = guowangbuliang.Guowangbuliang

jiajutiaozhengtaizhang_path =  'station_system/temp/jiajutiaozhengtaizhang.xlsx'
jiajutiaozhengtaizhang_model = jiajutiaozhengtaizhang.Jiajutiaozhengtaizhang




def main():
    pass
    import_excel_data(jiajutiaozhengtaizhang_path, jiajutiaozhengtaizhang_model)
    # query_set = station.Station.objects.all()
    # print(query_set)
    # temp = guowangbuliang.GuowangbuliangImage
    # temp.objects.all().delete()
    
    pass
    


if __name__ == '__main__':
    main()